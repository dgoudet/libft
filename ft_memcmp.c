/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/05 12:01:59 by dgoudet           #+#    #+#             */
/*   Updated: 2019/11/27 18:10:25 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_memcmp(const void *s1, const void *s2, size_t n)
{
	unsigned const char	*string1;
	unsigned const char	*string2;
	size_t				i;

	if (!n)
		return (0);
	string1 = s1;
	string2 = s2;
	i = 0;
	while (i < (n - 1) && string1[i] == string2[i])
		i++;
	return ((int)(string1[i] - string2[i]));
}
