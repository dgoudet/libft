/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/11 11:12:41 by dgoudet           #+#    #+#             */
/*   Updated: 2019/11/30 08:19:04 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_strcmp(char s1, const char *set)
{
	size_t i;

	i = 0;
	while (set[i])
	{
		if ((unsigned char)set[i] == (unsigned char)s1)
			return (1);
		i++;
	}
	return (0);
}

static int	ft_strlenbis(char const *s1, char const *set)
{
	int i;
	int len;

	i = 0;
	len = ft_strlen(s1);
	while (s1 && ft_strcmp(s1[i], set) == 1)
		i++;
	len--;
	while (s1 && ft_strcmp(s1[len], set) == 1)
		len--;
	return (len + 1 - i);
}

char		*ft_strtrim(char const *s1, char const *set)
{
	int		i;
	int		len;
	char	*s2;

	i = 0;
	if (!s1)
		return (NULL);
	if (!set || set[i] == '\0')
		return (s2 = ft_strdup(s1));
	while (s1 && ft_strcmp(s1[i], set) == 1)
		i++;
	if (s1[i] == '\0')
		return (s2 = ft_calloc(1, sizeof(*s2)));
	len = ft_strlenbis(s1, set);
	if ((s2 = malloc(sizeof(*s2) * (len + 1))) == NULL)
		return (NULL);
	s2 = ft_substr(s1, i, len);
	s2[len] = '\0';
	return (s2);
}
