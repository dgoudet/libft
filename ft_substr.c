/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/11 15:22:31 by dgoudet           #+#    #+#             */
/*   Updated: 2019/11/30 08:19:40 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	char			*s2;
	unsigned int	i;
	size_t			size;

	i = 0;
	if (!s)
		return (NULL);
	if (start > ft_strlen(s))
		return (s2 = ft_calloc(1, sizeof(*s2)));
	else
		size = ft_strlen(&s[start]);
	if (size > len)
		size = len;
	if ((s2 = malloc(sizeof(*s2) * (size + 1))) == NULL)
		return (NULL);
	while (i < size)
	{
		s2[i] = s[start];
		i++;
		start++;
	}
	s2[i] = '\0';
	return (s2);
}
