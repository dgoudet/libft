/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/06 16:40:09 by dgoudet           #+#    #+#             */
/*   Updated: 2019/11/28 10:34:28 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

static size_t	ft_strlen2(const char *s)
{
	size_t i;
	size_t k;

	k = 0;
	while (ft_isdigit(s[k]) == 0)
		k++;
	i = k;
	while (ft_isdigit(s[i]) == 1)
		i++;
	return (i - k);
}

static long		ft_calc_unit(const char *str)
{
	int		len;
	long	unit;

	unit = 1;
	len = ft_strlen2(str);
	while (len > 1)
	{
		unit = unit * 10;
		len--;
	}
	return (unit);
}

int				ft_atoi(const char *str)
{
	int		i;
	int		k;
	long	unit;
	long	res;

	i = 0;
	if (str[i] == 0)
		return (0);
	unit = ft_calc_unit(str);
	res = 0;
	k = 0;
	while (str[i] == ' ' || (str[i] >= 9 && str[i] <= 13))
		i++;
	k = i;
	if (str[i] == '+' || str[i] == '-')
		i++;
	while (ft_isdigit(str[i]) == 1)
	{
		res = res + (str[i] - 48) * unit;
		unit = unit / 10;
		i++;
	}
	if (str[k] == '-')
		res = res * -1;
	return (res);
}
