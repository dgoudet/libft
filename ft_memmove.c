/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/05 08:34:03 by dgoudet           #+#    #+#             */
/*   Updated: 2019/11/28 11:14:37 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t n)
{
	char			*d;
	const char		*s;
	const char		*ends;
	char			*endd;

	d = dst;
	s = src;
	if (d == 0 && s == 0)
		return (dst);
	if (d < s)
		while (n--)
			*d++ = *s++;
	else
	{
		ends = s + (n - 1);
		endd = d + (n - 1);
		while (n--)
			*endd-- = *ends--;
	}
	return (dst);
}
