/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/06 15:08:01 by dgoudet           #+#    #+#             */
/*   Updated: 2019/11/27 15:57:43 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t dstsize)
{
	size_t	j;
	size_t	k;
	size_t	dlen;
	size_t	slen;

	j = 0;
	if (!dst && !dstsize)
		return (ft_strlen(src));
	dlen = ft_strlen(dst);
	slen = ft_strlen(src);
	k = dlen;
	if (dstsize == 0)
		return (slen + dstsize);
	if (dstsize <= dlen)
		return (slen + dstsize);
	while (src[j] && (j < dstsize - dlen - 1))
	{
		dst[k] = src[j];
		j++;
		k++;
	}
	dst[k] = '\0';
	return (slen + dlen);
}
