#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

int main ()
{
	printf("%d", ft_strlen("\0"));
	printf("%d", strlen("\0"));
	printf("%d", ft_strlen("hello\n"));
	printf("%d", strlen("hello\n"));
	printf("%d", ft_strlen(NULL));
	printf("%d", strlen(NULL));
	printf("%d", ft_strcpy("\0"));
    printf("%d", strcpy("\0"));
    printf("%d", ft_strcpy("hello\n"));
    printf("%d", strcpy("hello\n"));
    printf("%d", ft_strcpy(NULL));
    printf("%d", strcpy(NULL));
	printf("%d", ft_strcmp("\0", "\0"));
    printf("%d", strcmp("\0", "\0"));
    printf("%d", ft_strcmp("hello\n", "hello"));
    printf("%d", strcmp("hello\n", "hello"));
	printf("%d", ft_strcmp("hello", "hello"));
    printf("%d", strcmp("hello", "hello"));
    printf("%d", ft_strcmp(NULL, "hi"));
    printf("%d", strcmp(NULL, "hi"));
	write(1, "hello", 5);
	ft_write("hello");
}




