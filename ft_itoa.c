/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/15 11:21:52 by dgoudet           #+#    #+#             */
/*   Updated: 2020/01/17 14:16:07 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	len_of_string(int n)
{
	int				len;
	unsigned int	nb;

	if (n < 0)
	{
		nb = n * -1;
		len = 2;
	}
	else
	{
		len = 1;
		nb = n;
	}
	while (nb > 9)
	{
		nb = nb / 10;
		len++;
	}
	return (len);
}

char		*ft_itoa(int n)
{
	char			*str;
	int				len;
	unsigned int	nb;

	len = len_of_string(n);
	if (n < 0)
		nb = n * -1;
	else
		nb = n;
	if ((str = malloc(sizeof(*str) * (len + 1))) == NULL)
		return (NULL);
	str[len] = '\0';
	len--;
	while (len >= 0)
	{
		if (len == 0 && n < 0)
			str[len] = '-';
		else
		{
			str[len] = (nb % 10) + 48;
			nb = nb / 10;
		}
		len--;
	}
	return (str);
}
