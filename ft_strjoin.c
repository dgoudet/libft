/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/23 13:32:42 by dgoudet           #+#    #+#             */
/*   Updated: 2019/11/30 08:19:59 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*s3;
	int		len;
	int		len2;
	int		lent;

	len = 0;
	len2 = 0;
	if (!s1 || !s2)
		return (NULL);
	if (s1)
		len = ft_strlen(s1);
	if (s2)
		len2 = ft_strlen(s2);
	lent = len + len2;
	if ((s3 = malloc(sizeof(*s3) * (lent + 1))) == NULL)
		return (NULL);
	if (s1 && len > 0)
		ft_memcpy(s3, s1, len);
	if (s2 && len2 > 0)
		ft_memcpy(&s3[len], s2, len2);
	s3[lent] = '\0';
	return (s3);
}
