/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/05 11:01:12 by dgoudet           #+#    #+#             */
/*   Updated: 2019/11/27 18:08:59 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	const char	*string;
	size_t		i;

	i = 0;
	string = s;
	while (i < n)
	{
		if (string[i] == c)
			return ((char *)&s[i]);
		else
			i++;
	}
	return (NULL);
}
