/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/22 11:53:53 by dgoudet           #+#    #+#             */
/*   Updated: 2019/11/30 08:56:57 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

static int	ft_nbs_else(char const *s, char c)
{
	int i;
	int j;

	i = 0;
	j = -1;
	while (s[i])
	{
		while (s[i] == c && s[i])
			i++;
		while (s[i] != c && s[i])
			i++;
		j++;
	}
	if (s[i - 1] != c)
		j++;
	return (j);
}

static int	ft_nbs(char const *s, char c)
{
	int i;

	i = 0;
	if (s[i] == '\0')
		return (0);
	while (s[i] == c)
		i++;
	if (s[i] == '\0')
		return (0);
	i = 0;
	while (s[i] && s[i] != c)
		i++;
	while (s[i] && s[i] == c)
		i++;
	if (s[i] == '\0')
		return (1);
	return (ft_nbs_else(s, c));
}

static char	**fill_tab(char **tab, const char *s, char c)
{
	int i;
	int j;
	int k;

	i = 0;
	j = 0;
	k = 0;
	while (s[i] && j < ft_nbs(s, c))
	{
		while (s[i] == c)
			i++;
		while (s[i] != c && s[i])
		{
			i++;
			k++;
		}
		tab[j] = ft_substr(s, (i - k), k);
		k = 0;
		j++;
	}
	return (tab);
}

char		**ft_split(char const *s, char c)
{
	char	**tab;
	int		nbs;

	if (!s)
		return (NULL);
	nbs = ft_nbs(s, c);
	if ((tab = malloc(sizeof(*tab) * (nbs + 1))) == NULL)
		return (NULL);
	if (nbs != 0)
		tab = fill_tab(tab, s, c);
	tab[nbs] = NULL;
	return (tab);
}
