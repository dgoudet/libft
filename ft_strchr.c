/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/05 15:59:40 by dgoudet           #+#    #+#             */
/*   Updated: 2019/11/28 11:41:33 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *s, int c)
{
	int		i;

	i = 0;
	if (s[i] == '\0' && c == '\0')
		return ((char *)&s[i]);
	while (s[i])
	{
		if (s[i] == c)
			return ((char*)&s[i]);
		if (s[i + 1] == c)
			return ((char *)&s[i + 1]);
		i++;
	}
	return (NULL);
}
